package cz.cvut.fel.ts1;

import java.util.stream.LongStream;

public class Math {
    private static final String negativeNumberExceptionMessageTemplate = "Can not compute value of factorial for a negative number. Given value: \"givenValue\"";

    public static String getNegativeNumberExceptionMessageFor(Integer number) {
        return negativeNumberExceptionMessageTemplate.replaceAll("givenValue", number.toString());
    }

    public static long factorial (int number) {
        if (number < 0) {
            throw new RuntimeException(getNegativeNumberExceptionMessageFor(number));
        }

        long result = 1;
        for (int i = 2; i <= number; i++) {
            result *= i;
        }
        return result;
    }

    public static long factorialUsingStreams(int number) {
        if (number < 0) {
            throw new RuntimeException(getNegativeNumberExceptionMessageFor(number));
        }

        return LongStream.rangeClosed(1, number)
                .reduce(1, (long x, long y) -> x * y);
    }

    public static long factorialRecursive(int number) {
        if (number < 0) {
            throw new RuntimeException(getNegativeNumberExceptionMessageFor(number));
        }

        if (number == 0) {
            return 1;
        }

        return number * factorialRecursive(number - 1);
    }
}
