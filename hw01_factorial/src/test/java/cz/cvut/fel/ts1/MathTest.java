package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;

import java.util.function.Function;

import static cz.cvut.fel.ts1.Math.getNegativeNumberExceptionMessageFor;
import static org.junit.jupiter.api.Assertions.*;

class MathTest {
    @Test
    public void testAllFactorialMethods() {
        testFactorialMethod(Math::factorial);
        testFactorialMethod(Math::factorialUsingStreams);
        testFactorialMethod(Math::factorialRecursive);
    }

    private void testFactorialMethod(Function<Integer, Long> factorialFunction) {
        assertEquals(factorialFunction.apply(0), 1);
        assertEquals(factorialFunction.apply(1), 1);
        assertEquals(factorialFunction.apply(2), 2);
        assertEquals(factorialFunction.apply(5), 120);
        assertThrows(RuntimeException.class, () -> factorialFunction.apply(-1), getNegativeNumberExceptionMessageFor(-1));
    }
}